# window.py
#
# Copyright 2018 Christopher Davis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gpg

from gi.repository import Gtk, GtkSource, GObject
from .gi_composites import GtkTemplate
from gpg.constants.sig import mode

GObject.type_ensure(GtkSource.View)

@GtkTemplate(ui='/org/gnome/Cypher/window.ui')
class CypherWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'CypherWindow'

    crypt_text = GtkTemplate.Child()
    recipient_menu_button = GtkTemplate.Child()
    recipient_list_box = GtkTemplate.Child()
    recipient_text = GtkTemplate.Child()
    buffer = NotImplemented
    context = gpg.Context(armor=True)
    encrypted_window = Gtk.Window()
    encrypted_label = Gtk.Label()
    recipient = NotImplemented

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.init_template()

        self.buffer = self.crypt_text.get_buffer()
        self.encrypted_window.set_title("Cipher Text")
        self.encrypted_label.set_selectable(True)
        self.encrypted_window.add(self.encrypted_label)
        self.encrypted_window.connect('destroy', self.on_encrypted_window_destroy)
        keylist = self.context.keylist()
        self.populate_list_box(keylist)

    @GtkTemplate.Callback
    def on_crypt_button_clicked(self, widget):
        buff_text = self.buffer.get_text(self.buffer.get_start_iter(), self.buffer.get_end_iter(), False)
        r = [self.recipient]
        enc, _, _ = self.context.encrypt(buff_text.encode('utf-8').strip(), r)
        print(enc.decode('utf-8', errors='ignore'))
        self.encrypted_label.set_text(enc.decode('utf-8'))
        self.encrypted_window.show_all()

    def populate_list_box(self, keylist):
         l = []
         last_radio = Gtk.RadioButton.new(None)
         radio = Gtk.RadioButton

         for key in keylist:
            radio = Gtk.RadioButton.new_with_label(None, key.uids[0].uid)
            radio.show()
            Gtk.RadioButton.join_group(radio, last_radio)
            last_radio = radio
            self.recipient_list_box.add(radio)
            radio.connect('toggled', self.on_radio_toggled, key)

    def on_radio_toggled(self, radio, key):
        if radio.get_active():
            self.recipient_text.set_text(radio.get_label())
            self.recipient = key

    def on_encrypted_window_destroy(self, window):
        window.hide()
